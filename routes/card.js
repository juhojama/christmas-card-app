const express = require('express');
const router = express.Router();

const fs = require('fs');

/* GET xmas card. */
router.get('/:cardID', function(req, res, next) {
  const cardID = req.params.cardID;
  img = cardID.toString();
  let rawData = fs.readFileSync('cards.json');
  let card = JSON.parse(rawData);
  if(!card[cardID]) {
    res.render('noCard');
  } else {
    res.render('card', {greeting: card[cardID]['greeting'],
                        img: img});
  }
});

module.exports = router;
