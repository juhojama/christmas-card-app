# Christmas Card App
## Demo
![Demo image](public/images/demo-card.jpg)

## Create card
1. add image to /images/ (.jpg only)
2. add greeting with image name to cards.json
eg.
```json 
{
      "greeting-xyz123":{ "greeting":"Merry Christmas!"}
}
```
3. npm install
4. npm start
5. card is presented in eg. localhost:3000/joulukortti/greeting-xyz123